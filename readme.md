# Blueprint
This is a package that will be used in our projects to interact with ChatGPT APIs.
## How to Use
1. Install the package with following command 
```sh
composer require kibocode/blueprint
```
2. List of available models 
```
$blueprint = new BluePrint(OPENAI_API_KEY);
$blueprint->listModels();
```

3. Retrieve a specific model
```
$blueprint->retrieveModel("davinci");
```
4. Text Completion request
```
$opts = [
        "prompt" => "Act like an email marketing expert and draft an email campaign.",
        "max_tokens" => 750,
        "temperature" => 0.9,
    ];
$response = $blueprint->completion($opts);
echo "<pre>"; print_r($response);
```
