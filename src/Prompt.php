<?php

namespace Kibocode\Blueprint;

use Exception;

class Prompt
{
    private static $prompt;
    private const FRAMEWORKS = [
        'AIDA'              => " Use the AIDA framework to write a persuasive product description that creates Awareness, Interest, Desire, and Action from the customer.",
        'Urgency-Patience'  => ' Create a sense of urgency for customers to take action, while also showing the patience and understanding of the brand.',
        'Emotion-Logic'     => ' Appeal to both emotions and logic by highlighting the emotional benefits of our product and the logical reasons to choose it.',
        'Personal-Universal'=> ' Make the product personal and relevant to the customer, while also showing how it solves a universal problem.',
        'Consistent-Contrasting' => ' Highlight the consistency and reliability of our product while contrasting it with the flaws of the competition.',
        'Story-Solve-Sell' => ' Tell a compelling story about a customer\'s problem and how our product solved it, leading to a successful sale.',
        'Solution-Savings-Social Proof' => ' Explain the solution our product offers, the cost savings it provides and include social proof to support our claims.',
        'Expectation-Surprise' => ' Set customer expectations and exceed them with surprising features and benefits.',
        'Exclusive-Inclusive' => ' Make customers feel special and exclusive, while still inclusive and welcoming.',
        'FOMO' => ' Create a sense of urgency and encourage customers to take action now before it\'s too late.',
        'Friend-expert' => ' Position our product as a trusted friend or expert, offering guidance and support to the customer.',
        'Niche-differentiation' => ' Explain why our product is unique and differentiates itself from competitors in the market.',
        'Social-Proof' => ' Incorporate customer testimonials and ratings to build trust and credibility for our product.',
        'Past-Present-Future' => ' Highlight the history and evolution of our product and explain how it is shaping the future.',
        'Before-After' => ' Showcase the transformation customers can expect after using our product with a before-after scenario.',
        'Unique-Value-Proposition' => ' Explain our brand\'s Unique Value Proposition in a persuasive and memorable way.',
        'Problem-Solution' => ' Describe a common problem faced by our target customers and how our product offers a solution.',
        'Pain-Agitate-Relief' => ' Highlight the pain points customers experience with traditional solutions and how our product provides relief.',
    ];

    private const POSITIONINGS = [
        'Confidence' => ' Emphasize the "Confidence" positioning model to show how our product helps individuals feel confident and empowered in their own skin.',
        'Innovation' => ' Utilize the "Innovation" positioning model to show how our product is constantly pushing the boundaries and setting new standards in the industry.',
        'Trend-setting' => ' Focus on the "Trend-Setting" positioning model to showcase how our product is always setting the latest trends and leading the way.',
        'Storytelling' => ' Highlight the "Storytelling" positioning model to show how our product has a story to tell, from its origin to its impact on the world.',
        'Emotional' => ' Leverage the "Emotional" positioning model to show how our product evokes feelings of confidence, happiness, and joy.',
        'Aspirational' => ' Use the "Aspirational" positioning model to showcase how our product represents a certain level of success and sophistication.',
        'Nostalgia' => ' Highlight the "Nostalgia" positioning model to show how our product evokes feelings of nostalgia and memories from the past.',
        'Experiential' => ' Leverage the "Experiential" positioning model to show how our product can enhance experiences and create memories.',
        'Heritage' => ' Focus on the "Heritage" positioning model to showcase the history and tradition behind our product brand.',
        'Functionality' => ' Utilize the "Functionality" positioning model to show how our product is designed to fit into a variety of different lifestyles and activities.',
        'Simplicity' => ' Use the "Simplicity" positioning model to show how our product is easy to wear and maintain, making it a go-to choice for busy individuals.',
        'Inclusivity' => ' Emphasize the "Inclusivity" positioning model to show how our product caters to a diverse range of sizes and body types.',
        'Comfort' => ' Highlight the "Comfort" positioning model to show how our product is not just stylish, but also comfortable to wear.',
        'Sustainability' => ' Leverage the "Sustainability" positioning model to showcase how our product is environmentally friendly and ethically made.',
        'Price-Sensitive' => ' Utilize the "Price" positioning model to show how our product offers a balance of affordability and luxury.',
        'Quality' => ' Use the "Quality" positioning model to showcase the materials and craftsmanship that goes into each piece of product.',
        'Exclusivity' => ' Highlight the "Exclusivity" positioning model by showcasing how our product line is unique and not available in other stores.',
        'Lifestyle' => ' Emphasize the "Lifestyle" positioning model by showing how our product fits into the ideal lifestyle of our target audience.',
    ];

    private const OFFERS = [
        'Free-Trials' => ' Offer customers a chance to try our product for free and describe the benefits of the free trial.',
        'Abandonment-Discounts' => ' Encourage customers to complete their purchase by offering a discount on abandoned carts.',
        'Pre-order discounts' => ' Encourage customers to pre-order new products by offering discounts and early access.',
        'Gift-Card' => ' Explain the benefits of purchasing a gift card for friends and family, and how it can be redeemed for products.',
        'Loyalty-Program' => ' Explain the loyalty program and the rewards customers can earn by making repeat purchases with our brand.',
        'Early-Access-Sale' => ' Make customers feel special by offering early access to new products and exclusive sales.',
        'Seasonal-Sale' => ' Explain the discounts available for products during the seasonal sale and how it can save customers money.',
        'Clearance-Sale' => ' Highlight the deep discounts and limited availability of products during the clearance sale.',
        'Limited-Edition' => ' Explain the unique features and benefits of the limited edition products and why they are in high demand.',
        'Flash-Sale' => ' Create a sense of urgency and highlight the deep discounts available during the flash sale.',
        'Exclusive-Access' => ' Make customers feel special and exclusive by offering them early access to new products or limited edition items.',
        'Free-give-with-every-order' => ' Showcase the free gift customers can receive with every order and how it adds value to their purchase.',
        'Bundle-Offer' => ' Explain the benefits of the bundle offer and how customers can save by purchasing multiple products together.',
        'Limited-Time-Discount' => 'Create a sense of urgency and highlight the limited time discount offer for customers.',
        'Referral-Bonus' => ' Encourage customers to refer their friends to our brand and describe the referral bonus they will receive.',
        'Free-Shipping' => ' Explain how customers can receive free shipping on their orders and the benefits of choosing our brand.',
        'Buy-one-get-one-free' => ' Highlight the Buy One Get One Free offer and the savings customers can make.',
    ];

    private const HOLIDAYS = [
        'Labor-Day' => ' Encourage customers to celebrate the end of summer by highlighting the discounts and promotions available on Labor Day.',
        'Independence-Day' => ' Encourage customers to celebrate independence and freedom by highlighting the discounts and promotions available on Independence Day.',
        'Halloween' => ' Encourage customers to get into the Halloween spirit by highlighting the discounts.',
        'Memorial-Day' => ' Encourage customers to remember and honor those who have served by highlighting the discounts and promotions available on Memorial Day.',
        'Fathers-Day' => ' Encourage customers to show love and appreciation to their fathers by highlighting the discounts and promotions available on Father\'s Day.',
        'Mothers-Day' => ' Encourage customers to show love and appreciation to their mothers by highlighting the discounts and promotions available on Mother\'s Day.',
        'Easter' => ' Explain how customers can save money on Easter gifts and decorations by shopping during the holiday season.',
        'Valentines-Day' => ' Highlight the popular gifts and promotions available for Valentine\'s Day and explain how customers can save money.',
        'New-Years-Day' => ' Encourage customers to start the new year off right by highlighting the discounts and promotions available on New Year\'s Day.',
        'Christmas' => ' Explain the benefits of shopping for gifts during the Christmas season and how customers can save money.',
        'Small-Business-Saturday' => ' Encourage customers to support small businesses by highlighting the discounts and promotions available on Small Business Saturday.',
        'Singles-Day' => ' Encourage customers to treat themselves on Singles Day by highlighting the discounts and promotions available.',
        'Black-Friday' => ' Create a sense of urgency and highlight the deep discounts available during Black Friday.',
        'Prime-Day' => ' Explain the benefits of shopping on Prime Day and how customers can save money on popular products.',
        'Cyber-Monday' => ' Highlight the discounts and promotions available for customers shopping online during Cyber Monday.',
    ];

    public static function getFrameworkPrompt($framework=null)
    {        
        return (!is_null($framework) && array_key_exists($framework,self::FRAMEWORKS)) ? self::FRAMEWORKS[$framework] : "";
    }
    
    public static function getPositioningPrompt($positioning=null)
    {
        return (!is_null($positioning) && array_key_exists($positioning,self::POSITIONINGS)) ? self::POSITIONINGS[$positioning] : "";
    }

    public static function getOffersPrompt($offer=null)
    {
        return (!is_null($offer) && array_key_exists($offer,self::OFFERS)) ? self::OFFERS[$offer] : "";
    }

    public static function getHolidaysPrompt($holiday=null)
    {
        return (!is_null($holiday) && array_key_exists($holiday,self::HOLIDAYS)) ? self::HOLIDAYS[$holiday] : "";
    }

    public static function getCompanyPrompt($company=null)
    {
        if(is_null($company)){
            return "";
        }
        return " from the company ". $company;
    }

    public static function getProductPrompt($product=null)
    {
        if(is_null($product)){
            return "";
        }
        return " for the product " . $product;
    }

    public static function getIndustryPrompt($industry=null)
    {
        if(is_null($industry)){
            return "";
        }
        return ", belonging to " . $industry . " industry";
    }

    public static function getWordsPrompt($words=null)
    {
        if(is_null($words)){
            return "";
        }
        return " Use up to ". $words ." of words.";
    }

    public static function getTargetAudiencePrompt($targetAudience=null)
    {
        if(is_null($targetAudience)){
            return "";
        }
        return " The campaign is intended for ". $targetAudience .".";
    }

    public static function sanitizeInputData($inputData)
    {
        $response = $inputData;
        $keys = ['framework','positioning','holiday','offer'];
        if(is_null($inputData)){
            return false;
        }
        foreach($inputData as $key => $value){
            if(in_array($key, $keys)){
                $modifiedValue = str_replace(' ','-',$value);
                $modifiedValue = str_replace('\'', '', $modifiedValue);
                $response[$key] = $modifiedValue;
            }
        }
        return $response;
    }

    public static function createPrompt($inputData=null)
    {
        if(is_null($inputData)){
            return false;
        }

        $inputData = self::sanitizeInputData($inputData);
    
        self::$prompt = "Act like an email marketing expert and draft an email campaign". self::getProductPrompt($inputData['brand']) . self::getCompanyPrompt($inputData['company']) . self::getIndustryPrompt($inputData['industry']) . ". The email should include a subject and a body." . self::getWordsPrompt($inputData['words']) . self::getTargetAudiencePrompt($inputData['target_audience']);
        self::$prompt .= self::getFrameworkPrompt($inputData['framework']);
        self::$prompt .= self::getPositioningPrompt($inputData['positioning']);
        self::$prompt .= self::getHolidaysPrompt($inputData['holiday']);
        self::$prompt .= self::getOffersPrompt($inputData['offer']);
        
        return self::$prompt;
    }
}